# Pizza Everywhere
This project exposes a couple of routes from a REST API with a very lightwight AngularJS frontend.
This was an experiment with a predefined stack. 
Before the project I had lightly used Ruby and never heard of the others.
It is intened to be an example of my ability to pick up a stack with which I am unfamiliar and implement a REST API with it.

The front end serves only as a means of viewing the data as an example of AJAX.

The stack is roughly as follows:
~~~
Front: AngularJS 1.7.x
Back: PostgreSQL, Sequel, Grape, Rack, Bundler, Ruby
~~~