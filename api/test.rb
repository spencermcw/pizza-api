require 'json'
require 'date'
require 'net/http'
require_relative 'api'

http = Net::HTTP
$host = 'http://localhost:9292'

# Prints the success or failure of an API Test
def api_assert(success, method, route)
  status = success ? "Successful" : "Failure to"
  puts "#{status} #{method} @ #{$host}#{route}"
end

# Prints the success or failure of an Generic Test
def func_assert(success, function, test_case)
  status = success ? "Successful" : "Failed"
  puts "#{status} execution of <#{function}> for test-case: \"#{test_case}\""
end


# TEST API Routes
begin
puts "Testing API Routes..."
# The API tests assume a seeded database!

# GET /people
people = JSON.parse(http.get(URI("#{$host}/people")))
api_assert(people.length > 0, 'GET', '/people')

# GET /pizzas
pizzas = JSON.parse(http.get(URI("#{$host}/pizzas")))
api_assert(pizzas.length > 0, 'GET', '/pizzas')

# GET /sales
sales = JSON.parse(http.get(URI("#{$host}/sales")))
api_assert(sales.length > 0, 'GET', '/sales')

# GET /sales/streaks
sales = JSON.parse(http.get(URI("#{$host}/sales/per_day")))
api_assert(sales.length > 0, 'GET', '/sales/per_day')

# GET /sales/streaks
streaks = JSON.parse(http.get(URI("#{$host}/sales/streaks")))
api_assert(streaks.length > 0, 'GET', '/sales/streaks')

# GET /sales/best_days
sales = JSON.parse(http.get(URI("#{$host}/sales/best_days")))
api_assert(sales.length > 0, 'GET', '/sales/best_days')

rescue Errno::ECONNREFUSED
  puts "ERR: Unable to reach API. Ensure you are hosting it at #{$host}"
end


puts "Testing API Logic..."

# TEST API Logic
puts "Testing Data Set 1"
# Single Record
data = [ { eaten_at: DateTime.new(2000, 1, 1) } ]
# Sales Per Day
result = sales_per_day(data)
func_assert(result.length == 1, 'sales_per_day', 'has correct length')
func_assert(result.last["sales"] == 1, 'sales_per_day', 'last element has correct amount of sales')
# Streaks
result = streaks(data)
func_assert(result.length == 1, 'streaks', 'has correct length')
func_assert(result[0].length == 1, 'streaks', 'first streak has correct length')
# Best Days
result = best_days(data)
func_assert(result.length == 1, 'best_days', 'has correct length')
func_assert(result.first["date"] == "2000-01-01", 'best_days', 'first month has correct day set')
func_assert(result.first["sales"] == 1, 'best_days', 'first month has correct number of sales')


puts "Testing Data Set 2"
# Different Days
data = [
  { eaten_at: DateTime.new(2000, 1, 1) },
  { eaten_at: DateTime.new(2000, 1, 2) },
  { eaten_at: DateTime.new(2000, 1, 3) },
  { eaten_at: DateTime.new(2000, 1, 3) },
  { eaten_at: DateTime.new(2000, 1, 4) },
]
# Sales Per Day
result = sales_per_day(data)
func_assert(result.length == 4, 'sales_per_day', 'has correct length')
func_assert(result.last["sales"] == 1, 'sales_per_day', 'last element has correct amount of sales')
# Streaks
result = streaks(data)
func_assert(result.length == 3, 'streaks', 'has correct length')
func_assert(result[0].length == 1, 'streaks', 'first streak has correct length')
# # Best Days
result = best_days(data)
func_assert(result.length == 1, 'best_days', 'has correct length')
func_assert(result.first["date"] == "2000-01-03", 'best_days', 'first month has correct day set')
func_assert(result.first["sales"] == 2, 'best_days', 'first month has correct number of sales')


puts "Testing Data Set 3"
# Different Years and Months
data = [
  { eaten_at: DateTime.new(2000, 1, 1) },
  { eaten_at: DateTime.new(2001, 1, 2) },
  { eaten_at: DateTime.new(2002, 2, 3) },
  { eaten_at: DateTime.new(2003, 1, 3) },
  { eaten_at: DateTime.new(2004, 1, 4) },
]
# Sales Per Day
result = sales_per_day(data)
func_assert(result.length == 5, 'sales_per_day', 'has correct length')
func_assert(result.last["sales"] == 1, 'sales_per_day', 'last element has correct amount of sales')
# Streaks
result = streaks(data)
func_assert(result.length == 5, 'streaks', 'has correct length')
func_assert(result[0].length == 1, 'streaks', 'first streak has correct length')
# Best Days
result = best_days(data)
func_assert(result.length == 5, 'best_days', 'has correct length')
func_assert(result.first["date"] == "2000-01-01", 'best_days', 'first month has correct day set')
func_assert(result.first["sales"] == 1, 'best_days', 'first month has correct number of sales')