require 'sequel'
require 'csv'

# Connect to DB
begin 
DB = Sequel.postgres('pizza', host: 'localhost', user: 'dev', password: 'password')
rescue
  puts "Unable to estabilsh connection to the DB with the following parameters:"
  puts "{ database: 'pizza', host: 'localhost', [port: 5432], user: 'dev', password: 'password' }"
  exit
end

puts 'DB Connection Established...'

# Datasets
People = DB[:people]
Pizzas = DB[:pizzas]
Sales = DB[:sales]



# MIGRATION
def migrate()
  # Create People Table
  DB.create_table :people do
    primary_key :id
    String :name
  end
  # Create Pizzas Table
  DB.create_table :pizzas do
    primary_key :id
    String :meat
  end
  # Create Sales Table
  DB.create_table :sales do
    primary_key :id
    foreign_key :person_id, :people
    foreign_key :pizza_id, :pizzas
    DateTime :eaten_at
  end
  puts 'DB migrated...'
end



# DROP TABLES
def drop_tables()
  DB.run 'DROP TABLE IF EXISTS sales'
  DB.run 'DROP TABLE IF EXISTS pizzas'
  DB.run 'DROP TABLE IF EXISTS people'
  puts 'Tables dropped...'
end



# SEEDING
def seed(file)
  data = CSV.read(file, headers: true)
  # Hash to ensure uniqueness
  names = {}
  pizzas = {}
  # Iterate over data
  for row in data do
    # Parse row
    name = row.field('person')
    meat = row.field('meat-type')
    date = row.field('date')
    # Conditional Insert new Person
    if not names.has_key?(name) then
      # Create Person
      person_id = People.insert(name: name)
      # Store ID of new Person
      names[name] = person_id
    end
    # Conditional Insert new Pizza
    if not pizzas.has_key?(meat) then
      # Create Pizza
      pizza_id = Pizzas.insert(meat: meat)
      # Store ID of new Pizza
      pizzas[meat] = pizza_id
    end
    # Insert Sales
    Sales.insert(person_id: names[name], pizza_id: pizzas[meat], eaten_at: date)
  end
  puts 'DB seeded...'
end
