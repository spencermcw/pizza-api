# config.ru

# Bundler
require 'rubygems'
require 'bundler/setup'
require 'rack/cors'

require_relative 'setup'
require_relative 'api'

use Rack::Cors do
  allow do
    origins '*'
    resource '*', :headers => :any, :methods => [:get, :post, :delete, :put, :options]
  end
end

run Pizza::API
