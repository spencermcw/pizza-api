require 'grape'
require_relative 'db'

# API LOGIC
def sales_per_day(sales)
  returning = [] 
  # Reduce as Hash first because it's O(1) lookup opposed to array.index O(n)
  spd = sales.reduce({}) do |sales_per_day, sale|
    key = sale[:eaten_at].to_date.to_s
    sales_per_day[key] = sales_per_day.has_key?(key) ? sales_per_day[key]+1 : 1
    sales_per_day
  end
  # Build array from Hash
  spd.each do |date, sales|
    returning.push({ "date" => date, "sales" => sales })
  end
  # Return
  return returning
end

def streaks(sales)
  returning = []
  curr_streak = []
  spd = sales_per_day(sales)
  for day in spd do
    # Hash to append
    if curr_streak.empty? then
      # Start Streak
      curr_streak.append(day)
      next
    end
    if day["sales"] > curr_streak.last["sales"] then
      # Continue Streak
      curr_streak.append(day)
    else
      # End Streak
      returning.append(curr_streak)
      curr_streak = [day]
    end
  end

  returning.append(curr_streak)
  # Return
  return returning
end

def best_days(sales)
  # Hash days based on month and compare
  best_day_per_month = sales_per_day(sales).reduce({}) do |best_days, day|
    date = Date.parse(day["date"])
    key = "#{date.year}-#{date.month}"
    # Create if key does not exist
    # Update if current day.sales > current best selling day this month
    best_days[key] = (best_days.has_key?(key) and best_days[key]["sales"] > day["sales"]) ?
      best_days[key] :
      { "date" => day["date"], "sales" => day["sales"] }
    best_days
  end
  # Removing temporary Keys
  returning = best_day_per_month.reduce([]) do |best_days, (month, values)|
    best_days.append({ "date" => values["date"], "sales" => values["sales"]})
    best_days
  end
  # Return
  return returning
end



# API MODULE
module Pizza
  class API < Grape::API
    format :json

    # These should be broken into :resources
    # but for the sake of this exercise I'm
    # just going to use hard-coded routes.

    # PEOPLE
    desc 'Return a list of all People.'
    params do
      optional :id
      optional :name
    end
    get '/people' do
      people = People
      # Chain where clauses for each param
      # Currently only handles equality
      declared(params).each do |column, value|
        if params[column] then
          people = people.where("#{column}": value)
        end
      end
      people.all
    end



    #PIZZAS
    desc 'Return list of all Pizzas.'
    params do
      optional :id
      optional :meat
    end
    get '/pizzas' do
      pizzas = Pizzas
      declared(params).each do |column, value|
        if params[column] then
          pizzas = pizzas.where("#{column}": value)
        end
      end
      pizzas.all
    end



    # SALES
    desc 'Return list of all Pizzas Consumed.'
    get '/sales' do
      Sales.join(:people, id: :person_id)
        .join(:pizzas, id: Sequel[:sales][:pizza_id])
        .select(:name, :eaten_at, Sequel[:meat].as(:pizza))
        .order(:eaten_at, :name).all
    end

    desc 'Return list of contiguous increased sales Streaks.'
    get '/sales/streaks' do
      sales = Sales.select(:eaten_at).order(:eaten_at).all
      streaks(sales)
    end

    desc 'Returns list of sales per day.'
    get '/sales/per_day' do
      sales = Sales.select(:eaten_at).order(:eaten_at).all
      sales_per_day(sales)
    end

    desc 'Return the day on which the most pizza was consumed for each month.'
    get '/sales/best_days' do
      sales = Sales.select(:eaten_at).order(:eaten_at).all
      best_days(sales)
    end

  end
end
