# Pizza Analytics REST API
> Built and tested using Ruby version 2.5.x

## Database Settings
A ROLE must be created as follows:
~~~
psql> CREATE ROLE dev WITH LOGIN SUPERUSER PASSWORD 'password';
~~~

Assumed variables for the PostgreSQL connection:
~~~
host: 'localhost'
port: default (5432)
database: 'pizza'
user: 'dev'
password: 'password'
~~~

## Execution
~~~
$ bundle install && rackup
~~~
Note: every execution of rackup will drop and repopulate the tables to adhere to the requisite, "ideally, typing `bundle install` and then `rackup config.ru` should launch the program".

## Testing
If you want to test the routes, start the rack server before execution.

If no server is found at the expected host, the test suite will fail the API tests gracefully and continue to test the API Logic.
~~~
$ ruby test.rb
~~~