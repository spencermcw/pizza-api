require_relative 'db'

# SETUP DATABASE
drop_tables()
migrate()
seed('data.csv')
