let URL = 'http://localhost:9292'

let app = angular.module('app', [])

app.controller('Controller', ($scope, $http) => {
  $scope.error = null
  $scope.name = 'albert'

  // Initialize $scope.pizzas for dropdown
  $scope.pizzas = []
  $http.get(URL+'/pizzas')
  .then(res => $scope.pizzas = res.data)
  .catch(e => console.error(e))

  $scope.routes = [
    '/people',
    '/pizzas',
    '/sales',
    '/sales/per_day',
    '/sales/streaks',
    '/sales/best_days'
  ]

  $scope.fetch = route => {
    $scope.fetchData(URL+route)
  }

  $scope.fetchPerson = name => {
    $scope.fetchData(URL+'/people?name='+name)
  }

  $scope.fetchPizza = meat => {
    $scope.fetchData(URL+'/pizzas?meat='+meat)
  }

  $scope.fetchData = url => {
    $scope.error = null
    $scope.data = []

    $http.get(url)
    .then(res => {
      if (res.data.length === 0)
        $scope.error = "No Records Found"
      $scope.data = res.data
    })
    .catch(e => $scope.error = e.data)
  }
})